
// We use the "require" directive to load Node.js modules.
// A "module" is a software component or part of a program that contains one or more routines
// "http modules" it lets the Node.js to transfer data using the "Hypertext Transfer Protocol"
// HTTP is a protocol that allows the fetching of resources such as HTML Documents.
// Client(browser) and Servers(node JS/Express JS) communicate by exchanging individual messages.
	// Requests -> sent by client/web browsers
	// Responses -> answer sent by the servers
let http = require("http");

// Using this module's createServer() method, we can create an HTTP sever that listens to request on a specified port and gives responses back to the client.
// The arguments passed in the function are requests and responses objects (data type) that contains methods that allow us to receive requests from the client and send response back from the server.
// A port is a virtual point where a network start and end.
http.createServer(function(request, response){
	// Use the writeHead() method to:
	// Set a status code for the response - a 200 means OK (HTTP Status response).
	// Set the content-type of the response as a plain text message.
	response.writeHead(200, {"Content-Type":"text/plain"});

	// Send the response with the text content "Hello World"
	response.end("Hello World");
}).listen(4000);

// When server is running, a console message will be printed.
console.log("Server running at localhost:4000");

/*
	
	Mini activity:

	Create another js file named "routes.js" and create a different server. In the server response print "Hello Again World".


	Send a screenshot of your work in the batch hangouts.

*/